

## Slow boot

Oplever du at din ubuntu maskine er lang tid om boot.

![image test](Pasted_image_20230902214530.png)

Ved flere netkort kan det være en fordel at sætte følgende i netplan config filen:

```bash
sudo nano /etc/netplan/00-installer-config.yaml
```


![nano editor](Pasted_image_20230902220841.png)
farver og linjenummer se [[#Nano Editor styling]]


```bash
 1 # This is the network config written by 'subiquity'
 2 network:
 3   ethernets:
 4     eth0:
 5       dhcp4: true
 6     eth1:
 7       optional: true
 8       dhcp4: false
 9     eth2:
10       optional: true
11       dhcp4: false
12   version: 2
13
14
```
Vigtig husk indryk er spaces not tabs!

[netplan doc](https://netplan.readthedocs.io/en/stable/)

## Password all the time

I et lab kan det til tider spare ldit tid ikke at skulle indtaste password de gange man vil bruge sin sudo rettighed. 

**OBS GØR KUN DETTE I ET LAB**.

Derfor kan det være en idé at tilføje en linje i sudoers med følgende:

```bash
[brugernavn] ALL=(ALL) NOPASSWD:ALL
```

![sudoers file](sudoers_NOPASSWD.png)
farver og linjenummer se [[#Nano Editor styling]]

## Update / upgrade trick

Den første kommando jeg kørte var:

```bash
sudo apt update && sudo apt upgrade -y
```

Det kan jeg fx. se ved at bruge commandoen `history` det giver følgende output

```bash
hans@ubuntu-2204-a:~$ history

    1  sudo apt update && sudo apt upgrade -y
    2  poweroff
    3  ls -al
    4  cat /etc/nanorc
    5  cp /etc/nanorc /home/hans/.nanorc
    6  nano .nanorc
    7  sudo nano /etc/nanorc
    8  nano .nanorc
    9  nano test.txt
   10  sudo nano test.txt
```

Derfor kan jeg når jeg er logget ind nøjes med at udføre den første kommando i min history igen ved at bruge `!1` udråbstegn 1

```bash
hans@ubuntu-2204-a:~$ !1
sudo apt update && sudo apt upgrade -y
Hit:1 http://dk.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://dk.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:3 http://dk.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:4 http://dk.archive.ubuntu.com/ubuntu jammy-security InRelease
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
hans@ubuntu-2204-a:~$
```

## Nano Editor styling

Der er selvfølgelig smag og behag men jeg kan godt li at ændre på lidt ved nano editoren, lidt farver, linjenummer og tabstospaces som især er nyttig ved yaml og netplan configs.

[more nano](https://www.nano-editor.org/)

config sker i .nanorc filen
[https://www.nano-editor.org/dist/latest/nano.html#Nanorc-Files](https://www.nano-editor.org/dist/latest/nano.html#Nanorc-Files)

Først laver vi en kopi af system-wide nanorc til vores home folder

```bash
cp /etc/nanorc /home/hans/.nanorc
```

herefter ændre jeg som minimum i system-wide nanorc:

```
set autoindent
set linenumbers
set tabsize 4
set tabstospaces
```

Herefter sætter de valgte farver til root sådan at jeg kan visuelt se forskel på om jeg er i root nano eller alm. bruger.

I bruger .nanorc filen sætter jeg de "normale" farver.





